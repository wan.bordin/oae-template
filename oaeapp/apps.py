from django.apps import AppConfig


class OaeappConfig(AppConfig):
    name = 'oaeapp'
