from django.shortcuts import render
import random

# Create your views here.
def indexPage(request):
    return render(request, 'index.html')

def indexOaePage(request):
    return render(request, 'index_oae.html')

def HomePage(request):
    return render(request, 'home.html')

def DetailPage(request):
    return render(request, 'detail.html')